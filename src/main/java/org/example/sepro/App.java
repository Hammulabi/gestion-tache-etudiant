package org.example.sepro;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        // Exemple d'utilisation

        // Les dates
        System.out.println("---------------Les dates-----------------");
        Date maDate = new Date();
        Date maDate2 = new Date(19, 12, 2023);
        maDate.afficherDate();
        maDate2.afficherDate();
        System.out.println("\n");

        // Les tâches
        System.out.println("---------------Les tâches-----------------");
        Task task1 = new Task();
        Task task2 = new Task("Tests Unitaires", "Apprendre à coder avec les TU", false, 25, 12, 2023);
        task1.afficherTache();
        task2.setTerminee(true);
        task2.afficherTache();
        System.out.println("\n");

        //Avec le TaskManager
        System.out.println("---------------Task Manager-----------------");
        TaskManager myTaskManager = new TaskManager();
        Task maTache = new Task("Faire les courses", "Acheter des fruits et légumes", false, 14, 12, 2023);
        Task maTache2 = new Task("Dry January", "retrouver mon corps d'athlète", false, 25, 12, 2023);
        myTaskManager.addTask(maTache);
        myTaskManager.addTask(maTache2);

        List<Task> myTasks = new ArrayList<>();
        myTasks = myTaskManager.getTasks();
        for (Task taskm : myTasks) {
            System.out.println("---------------Tâche-----------------");
            taskm.afficherTache();
            System.out.println("\n");
        }
    }
}
