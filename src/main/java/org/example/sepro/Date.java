package org.example.sepro;

public class Date {
    private int jour;
    private int mois;
    private int annee;

    // Constructeur par défaut
    public Date() {
        // Initialisation des valeurs par défaut
        this.jour = 1;
        this.mois = 1;
        this.annee = 2024;
    }

    // Constructeur surchargé
    public Date(int jour, int mois, int annee) {
        this.jour = jour;
        this.mois = mois;
        this.annee = annee;
    }

    // Méthode pour afficher la date
    public void afficherDate() {
        System.out.println("Date: " + jour + "/" + mois + "/" + annee);
    }

    // Getters et setters pour les attributs
    public int getJour() {
        return jour;
    }

    public void setJour(int jour) {
        this.jour = jour;
    }

    public int getMois() {
        return mois;
    }

    public void setMois(int mois) {
        this.mois = mois;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }
}
