package org.example.sepro;

public class Task {
    private String name;
    private String description;
    private boolean terminee;
    private Date date;

    public Task() {
        this.name = "name_by_default";
        this.description = "description_by_default";
        this.terminee = false;
        this.date = new Date();
    }

    public Task(String name, String description, boolean terminee, int jour, int mois, int annee) {
        this.name = name;
        this.description = description;
        this.terminee = terminee;
        this.date = new Date(jour, mois, annee);
    }


    // Getter et Setter pour l'attribut "name"
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate()   {
        return this.date; 
    }

    // Getter et Setter pour l'attribut "description"
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // Getter et Setter pour l'attribut "terminee"
    public boolean isTerminee() {
        return terminee;
    }

    public void setTerminee(boolean terminee) {
        this.terminee = terminee;
    }

    public void afficherTache() {
        System.out.println("Nom de la tâche: " + name);
        System.out.println("Description: " + description);
        System.out.println("Tâche terminée: " + terminee);
        if (date != null) {
            System.out.print("Date de la tâche: ");
            date.afficherDate();
        }
    }
}