// src/main/java/com/example/TaskManager.java
package org.example.sepro;

import java.util.ArrayList;
import java.util.List;

public class TaskManager {
    private List<Task> tasks;

    public TaskManager() {
        tasks = new ArrayList<>();
    }

    public boolean addTask(Task task) {
        if (tasks.add(task)) {
            return true;
        }
        else {
            return false; 
        }
    }

    public List<Task> getTasks() {
        return tasks;
    }

}
